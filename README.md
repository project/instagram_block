# Instagram Block

This is a very simple module that integrates with Instagram and creates a
block containing your most recent Instagram posts.

The block's configuration page lets you choose how many posts and what size
they should appear in the block. The images are individually exposed to the
drupal theme layer, so developers have access to an all of the variables
provided by the Instagram API should they choose to extent the block.

For more informations see the
[Instagram developer pages](http://instagram.com/developer/endpoints/users/#get_users_media_recent).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/instagram_block).

To submit bug reports and feature suggestions, or to track changes
[issue queue](https://www.drupal.org/project/issues/instagram_block).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

As of the beginning to use the Instagram basic Display API we incorporate the
Esspresso dev [Instagram basic api](https://github.com/espresso-dev/instagram-basic-display-php)
see about using composer to install that in the next section


## Installation

This version of this module requires espresso-dev instagram basic display api.
User cola first mentioned it in this
[issue 3119309](https://www.drupal.org/project/instagram_block/issues/3119309)
and it refers to the espresso-dev github archive above.

Install it with:

`composer require espresso-dev/instagram-basic-display-php`

You'll also need to get set up with a facebook developer account, use that to
create a Facebook App, provide a 'platform' for Instagram Basic Display App.
Then you can get an AppId and an App secret for use with this module.
Here are [instructions](https://developers.facebook.com/docs/instagram-basic-display-api/getting-started)

You need to use the Instagram App ID , not the Facebook App ID.

In the 'Valid OAuth Redirect URIs', include
`https://your-domain.com/instagram_block/callback/` with trailing slash.
This url is accessible only to users with administer permissions.

Instagram requires https to be set up on your site.

Install as you would normally install a contributed Drupal module. See
[Installing Drupal Modules](https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules)
for further information.


## Configuration

To add Instagram AppId and App Secret information, Go to configuration page for
Instagram Block i.e
`Home-> Administration-> Configuration-> Content authoring: /admin/config/content/instagram_block`

After entering the AppId and App Secret, and saving, you can click the refresh
access token link. You shouldnt normally have top do this- the token should be
replaced automatically via cron once it is within a month of the token
expiration.

To add Instagram Block to specific content region Go to Block Layout.
i.e
`Home-> Administration-> Structure-> Block layout`. You can configure the size
and type of image there.

The module uses a stored config value, the Instagram lt_token, a token issued
by instagram after Oauth exchange, which lasts for 60 days.

It will be refreshed with cron job within 30 days of expiry (this is not yet
effectively tested)

Updates to this particular version by Jim Earl (jimurl)


## Maintainers

- Yan Loetzer (yanniboi) - https://www.drupal.org/u/yanniboi
